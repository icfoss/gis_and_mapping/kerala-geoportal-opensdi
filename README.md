# Kerala GeoPortal - opensdi


The Kerala Geoportal project is a tailored adaptation of the GeoNode framework for the KSDI team, focusing on the unique spatial data infrastructure needs of Kerala. A key highlight is the implementation of a custom tile server, enhancing map rendering capabilities and potentially optimizing performance. Complemented by user interface modifications, the platform ensures a user-friendly experience specifically designed to meet the requirements of the KSDI team. Leveraging the GeoNode base provides a solid foundation for managing and sharing geospatial data efficiently within the context of Kerala's spatial data ecosystem.
[Kerala Geoportal](https://opensdi.kerala.gov.in/)

![KSDI-OPENSDI](Images/opensdi-ksdi.png)


# Install
[To install the Geonode project, follow this documentation.](Install.md)

## Customizing OpenSDI Installation

To apply the specified customizations to the newly installed OpenSDI, follow these steps:

1. **Replace Contents in Installation Folder:**
    - If the installation was done without Docker, you may replace the contents directly in the installation folder.

2. **Docker Installation:**
    - If the installation was done using Docker, you have two options:
        - **Replace Before Docker Builds:**
            - Modify the content before initiating the Docker build process to ensure the customizations are incorporated.
        - **Replace After Installation:**
            - Change the content post-installation by replacing files inside the Docker container of `django for geonode`.

Ensure you carefully replace files, considering version compatibility and backup procedures. Always document changes made to maintain transparency and facilitate future updates.

