# GeoNode Installation [Centos 7]

[ Reference geonode-project](https://github.com/GeoNode/geonode-project)


### Update system packages
```bash
yum update -y
```

### Install Python 3
```bash
yum install -y python3
```

### Install Django 3.2.16 using pip
```bash
pip3 install Django==3.2.16
```

### Install Docker dependencies
```bash
yum install -y yum-utils

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

### Start Docker service
```bash
systemctl start docker
```

### Check Docker service status
```bash
systemctl status docker
```

### Install docker-compose
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version
```

### Clone GeoNode project repository (using specific branch)
```bash
yum install git

git clone https://github.com/GeoNode/geonode-project.git -b 4.1.x
```

**NOTE : Uncomment the logstash and LDAP dependencies installation procedures from the Dockerfile before creating a project using the geonode-project template.**

### Start a new project using the cloned GeoNode project (eg : opensdi)
```bash
django-admin startproject --template=./geonode-project -e py,sh,md,rst,json,yml,ini,env,sample,properties -n monitoring-cron -n Dockerfile opensdi
```

### Change to the project directory
```bash
cd opensdi
```

### Create an environment file and add configurations correctly
```bash
vi .env
```

### Build Docker containers (with no cache)
```bash
docker compose build --no-cache
```

### Start Docker containers in detached mode
```bash
docker compose up -d
```
